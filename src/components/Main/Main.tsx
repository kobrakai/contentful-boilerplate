import React from 'react'
import Container from 'components/Container'
import * as Styles from './styles'

interface Props {
	headerHeight: number
	footerHeight: number
	children: React.ReactNode
}

const Main: React.FC<Props> = ({
	headerHeight,
	footerHeight,
	children
}: Props) => (
	<Styles.Main headerHeight={headerHeight} footerHeight={footerHeight}>
		{children}
	</Styles.Main>
)

export default Main
