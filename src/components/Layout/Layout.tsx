import React, { useState, useRef, useEffect } from 'react'
import SkipLinks from 'components/SkipLinks'
import Header from 'components/Header'
import Main from 'components/Main'
import Footer from 'components/Footer'

interface Props {
	path: string
	context: {
		id: string
		locale: string
	}
	children: React.ReactNode
}

const Layout: React.FC<Props> = ({ path, context, children }: Props) => {
	const [headerHeight, setHeaderHeight] = useState(0)
	const [footerHeight, setfooterHeight] = useState(0)
	const headerElement = useRef<HTMLDivElement>(null)
	const footerElement = useRef<HTMLDivElement>(null)

	useEffect(() => {
		setHeaderHeight(headerElement.current.clientHeight)
		setfooterHeight(footerElement.current.clientHeight)
	}, [headerElement, footerElement])

	return (
		<>
			<SkipLinks />

			<Header ref={headerElement} path={path} context={context} />

			<Main headerHeight={headerHeight} footerHeight={footerHeight}>
				{children}
			</Main>

			<Footer ref={footerElement} />
		</>
	)
}

export default Layout
