import styled, { css } from 'styled-components'
import { Link as GatsbyLink } from 'gatsby'
import { ThemeProps } from 'src/theme'

interface Props extends React.AnchorHTMLAttributes<HTMLAnchorElement> {
	theme?: ThemeProps
}

const sharedStyles = () => css`
	cursor: pointer;
`

export const ExternalLink = styled.a<Props>`
	${sharedStyles}
`

export const InternalLink = styled(GatsbyLink)<Props>`
	${sharedStyles}
`
