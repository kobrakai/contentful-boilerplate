import {} from 'styled-components'
import Theme from 'theme/index'

declare module 'styled-components' {
	type BaseTheme = typeof Theme;
	export interface DefaultTheme extends BaseTheme {}
}
