import React from 'react'
import renderer from 'react-test-renderer'
import LocaleSwitcher from 'components/LocaleSwitcher'
import '../../i18n/index'

describe('Locale switcher', () => {
	it('renders correctly', () => {
		const path = '/'
		const context = {
			id: 'fec91240-366c-519d-aa1b-e2785b67df15',
			locale: 'en'
		}

		const tree = renderer
			.create(<LocaleSwitcher path={path} context={context} />)
			.toJSON()

		expect(tree).toMatchSnapshot()
	})
})
