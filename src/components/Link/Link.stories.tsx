import React from 'react'
import { Story, Meta } from '@storybook/react/types-6-0'
import Link, { Props } from './Link'

export default {
	title: 'Components/Link',
	component: Link
} as Meta

const Template: Story<Props> = ({ children, ...rest }) => (
	<Link {...rest}>{children}</Link>
)

export const Default = Template.bind({})

Default.args = {
	locale: 'nl',
	to: '#',
	onClick: (event) => event.preventDefault(),
	children: 'Link label'
}

Default.argTypes = {
	children: {
		name: 'label',
		type: { name: 'string', required: true },
		description: 'Link text/label'
	},
	to: {
		description: 'Link to an internal/external URL'
	},
	locale: {
		description: 'Force the internal URL to a different locale'
	}
}
