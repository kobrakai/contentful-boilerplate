interface BreakpointsProps {
  [key: string]: string
}

const Breakpoints: BreakpointsProps = {
  xs: '20em', // 320 pixels
  s: '30em', // 480 pixel
  m: '48em', // 768 pixels
  l: '64em', // 1024 pixels
  xl: '80em', // 1280 pixels
  xxl: '90em' // 1440 pixels
}

export { BreakpointsProps, Breakpoints }
