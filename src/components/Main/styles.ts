import styled from 'styled-components'
import { ThemeProps } from 'theme/index'

interface Props {
  headerHeight: number
  footerHeight: number
  theme: ThemeProps
}

export const Main = styled.main<Props>`
  ${({ headerHeight, footerHeight }) => `
    margin-top: ${headerHeight}px;
    padding: 0 0 2.5em;
    min-height: calc(100vh - ${footerHeight}px - ${headerHeight}px);
  `}
`
