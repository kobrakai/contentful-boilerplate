interface SpacingProps {
  [key: string]: string
}

const Spacing: SpacingProps = {
  xs: '0.25rem',
  s: '0.5rem',
  m: '1rem',
  l: '2rem',
  xl: '3rem',
  xxl: '4rem',
  xxl2: '8rem'
}

export { SpacingProps, Spacing }
