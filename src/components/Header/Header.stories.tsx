import React, { useRef } from 'react'
import { Meta } from '@storybook/react/types-6-0'
import Header from './Header'

export default {
	title: 'Components/Header',
	component: Header
} as Meta

export const Default = () => {
	const headerElement = useRef<HTMLDivElement>(null)
	const path = '/'
	const context = {
		id: 'fec91240-366c-519d-aa1b-e2785b67df15',
		locale: 'en'
	}

	return <Header ref={headerElement} path={path} context={context} />
}
