import React from 'react'
import * as Styles from './styles'

export interface ContainerProps extends React.HTMLAttributes<HTMLDivElement> {
  xs?: boolean
  sm?: boolean
  md?: boolean
  lg?: boolean
  xl?: boolean
  children: any
}

export const Container: React.FC<ContainerProps> = ({
  children,
  ...rest
}) => (
  <Styles.Container {...rest}>
    { children }
  </Styles.Container>
)

Container.defaultProps = {
	xs: false,
	sm: false,
	md: true,
	lg: false,
	xl: false
}

export default Container




  