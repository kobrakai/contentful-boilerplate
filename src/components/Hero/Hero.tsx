import React from 'react'
import Img, { FixedObject, FluidObject } from 'gatsby-image'
import RichText from 'components/RichText'
import { Container, Row, Col } from 'components/Grid'
import * as Styles from './styles'

interface Description {
	raw?: string
}

interface Image {
	title?: string
	description?: string
	fluid?: FluidObject | FluidObject[]
	fixed?: FixedObject | FixedObject[]
}

export interface HeroProps {
	title: string
	content?: string | Description
	image: Image
}

const Hero: React.FC<HeroProps> = ({
	title,
	content,
	image,
	...rest
}: HeroProps) => {
	const alt = image.title || image.description || title

	const getContent = () => {
		if (content && typeof content === 'string') {
			return content
		} else if (content && typeof content === 'object') {
			return JSON.parse(content.raw)
		}
	}

	return (
		<Styles.Wrapper {...rest}>
			<Img
				fluid={image.fluid}
				alt={alt}
				title={alt}
			/>

			<Styles.Container md>
				<Row>
					<Col md={8}>
						<RichText
							heading="h1"
							title={title}
							content={getContent()}
						/>
					</Col>
				</Row>
			</Styles.Container>
		</Styles.Wrapper>
	)
}

export default Hero
  