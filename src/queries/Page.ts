import { graphql } from 'gatsby'

export const PageFragment = graphql`
	fragment PageFragment on ContentfulPage {
		id
		slug
		node_locale
		title
		metaTitle
		metaRobots
		metaDescription {
			metaDescription
		}
		richDescription: description {
			raw
		}
		image {
			title
			description
			fluid {
				src
				srcSet
				sizes
				aspectRatio
			}
		}
	}
`
