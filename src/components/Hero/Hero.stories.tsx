import React from 'react'
import { Meta } from '@storybook/react/types-6-0'
import Hero from './Hero'

export default {
	title: 'Components/Hero',
	component: Hero
} as Meta

export const Default = ({ title, content, image }) => <Hero title={title} content={content} image={image} />

Default.parameters = {
	title: 'Hero',
	content: 'Test',
	image: ''
}
      