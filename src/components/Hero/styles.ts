import styled from 'styled-components'
import Theme, { ThemeProps } from 'theme/index'
import { Container as BaseContainer } from 'components/Grid'

interface Props {
  theme: ThemeProps
}

export const Wrapper = styled.div<Props>`
	position: relative;

	@media (min-width: ${Theme.breakpoints.l}) {
		height: clamp(25rem, calc(100vh - 4.376rem), 60rem);
	}

	> .gatsby-image-wrapper {
		width: 100%;
		height: 100%;
		object-fit: cover;
	}

	&::before {
		@media (min-width: ${Theme.breakpoints.l}) {
			content: '';
			position: absolute;
			top: 0;
			bottom: 0;
			right: 0;
			left: 0;
			background-image: linear-gradient(45deg, rgba(255, 255, 255, 0), rgba(255, 255, 255, .1) 50%, rgba(255, 255, 255, .5));
			backdrop-filter: blur(1rem);
			z-index: 1;
			right: 25%;
			clip-path: polygon(0 0, 100% 0, calc(100% - 10rem) 100%, 0 100%);
		}

		@media (min-width: ${Theme.breakpoints.xxl}) {
			right: 30%;
		}
	}
`

export const Container = styled(BaseContainer)`
	padding-top: 1rem;
	padding-bottom: 1rem;

	@media (min-width: ${Theme.breakpoints.l}) {
		position: absolute;
		top: 50%;
		left: 50%;
		transform: translate(-50%, -50%);
		z-index: 1;
	}
`

