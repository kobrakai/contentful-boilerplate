import React from 'react'
import { Meta } from '@storybook/react/types-6-0'
import LocaleSwitcher from './LocaleSwitcher'

export default {
	title: 'Components/LocaleSwitcher',
	component: LocaleSwitcher
} as Meta

export const Default = () => {
	const path = '/'
	const context = {
		id: 'fec91240-366c-519d-aa1b-e2785b67df15',
		locale: 'en'
	}

	return <LocaleSwitcher path={path} context={context} />
}
