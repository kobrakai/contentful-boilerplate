import React from 'react'
import renderer from 'react-test-renderer'
import Main from 'components/Main'
import '../../i18n/index'

describe('Main content', () => {
	it('renders correctly', () => {
		const tree = renderer
			.create(<Main headerHeight={69} footerHeight={225} />)
			.toJSON()
		expect(tree).toMatchSnapshot()
	})
})
