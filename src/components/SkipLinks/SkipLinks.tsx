import React from 'react'
import { useTranslation } from 'react-i18next'
import Container from 'components/Container'
import * as Styles from './styles'

const SkipLinks = () => {
	const { t } = useTranslation()

	return (
		<Container>
			<Styles.Wrapper id="skiplinks">
				<Styles.Link variant="secondary" to="#navigation">
					{t('navigation')}
				</Styles.Link>

				<Styles.Link variant="secondary" to="#main">
					{t('mainContent')}
				</Styles.Link>

				<Styles.Link variant="secondary" to="#footer">
					{t('footer')}
				</Styles.Link>
			</Styles.Wrapper>
		</Container>
	)
}

export default SkipLinks
