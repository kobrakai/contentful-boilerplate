import React from 'react'
import * as Styles from './styles'

type NumberAttr =
  | number
  | '1'
  | '2'
  | '3'
  | '4'
  | '5'
  | '6'
  | '7'
  | '8'
  | '9'
  | '10'
  | '11'
  | '12'

type ColOrder = 'first' | 'last' | NumberAttr
type ColSize = boolean | 'auto' | NumberAttr
type ColSpec =
  | ColSize
  | { span?: ColSize; offset?: NumberAttr; order?: ColOrder }
  
export interface ColProps extends React.AnchorHTMLAttributes<HTMLDivElement> {
  xs?: ColSpec
  sm?: ColSpec
  md?: ColSpec
  lg?: ColSpec
  xl?: ColSpec
}

export const Col: React.FC<ColProps> = ({
  children,
  ...rest
}) => (
  <Styles.Col {...rest}>
    { children }
  </Styles.Col>
)

export default Col



  