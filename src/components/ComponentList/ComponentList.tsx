import React, { useState, useEffect } from 'react'
import loadable from '@loadable/component'
import ErrorBoundary from './ErrorBoundary'

const AsyncComponent = loadable(({ name }) => import(`../${name}`));

export interface ItemProps {
  __typename: string
  id: string
}
interface Props {
  components: Array<ItemProps>
}

const ComponentList: React.FC<Props> = ({
  components
}: Props) => {
  const [reactComponents, setReactComponents] = useState([]);

  useEffect(() => {
    if (components) {
      const classComponents = components.map(item => {
        const { __typename, id } = item

        return {
          name: __typename && __typename.replace('Contentful', ''),
          id,
          forwardedProps: item
        }
      })

      setReactComponents(classComponents)
    }
  }, [components])

  return (
    <>
      {reactComponents &&
        reactComponents.map((component, index) => (
          <ErrorBoundary key={index}>
            {component.name ? (
              <AsyncComponent
                name={component.name}
                componentId={component.id}
                {...component.forwardedProps}
              />
            ) : <></>}
          </ErrorBoundary>
        ))}
    </>
  )
}

export default ComponentList
