import React from 'react'
import { Story, Meta } from '@storybook/react/types-6-0'
import Button, { Props } from './Button'

const args = {
	children: 'Button label',
	size: 'l',
	variant: 'primary',
	to: '#',
	locale: 'nl'
}

const argTypes = {
	size: {
		type: { name: 'string', required: true },
		description: 'Button size from small to large',
		table: {
			defaultValue: { summary: 'L' }
		},
		control: {
			type: 'select',
			options: ['s', 'm', 'l']
		}
	},
	variant: {
		type: { name: 'string', required: true },
		description: 'Button theme color variant',
		table: {
			defaultValue: { summary: 'primary' }
		},
		control: {
			type: 'select',
			options: ['primary', 'secondary', 'white']
		}
	},
	children: {
		name: 'label',
		type: { name: 'string', required: true },
		description: 'Button text/label'
	},
	to: {
		description: 'Link to an internal/external URL'
	},
	locale: {
		description: 'Force the internal URL to a different locale'
	}
}

export default {
	title: 'Components/Button',
	component: Button
} as Meta

const Template: Story<Props> = ({ children, ...rest }) => (
	<Button {...rest}>{children}</Button>
)

export const Primary = Template.bind({})
Primary.args = args
Primary.argTypes = argTypes
Primary.parameters = {
	docs: {
		description: {
			story:
				'Main call to action button. Can be used for internal and external links. Optionally can be used for custom click events'
		}
	}
}

export const Secondary = Template.bind({})
Secondary.args = { ...args, variant: 'secondary' }
Secondary.argTypes = argTypes
Secondary.parameters = {
	docs: {
		description: {
			story:
				'Secondary call to action button. Can be used for internal and external links. Optionally can be used for custom click events'
		}
	}
}

export const White = Template.bind({})
White.args = { ...args, variant: 'white' }
White.argTypes = argTypes
White.parameters = {
	docs: {
		description: {
			story:
				'White call to action button. Can be used for internal and external links. Optionally can be used for custom click events'
		}
	}
}
