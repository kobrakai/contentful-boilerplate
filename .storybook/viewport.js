const newViewports = [
  {
    name: 'Responsive',
    styles: {
      width: '100%',
      height: '100%',
    },
    type: 'desktop'
  },
  {
    name: 'iPad pro',
    styles: {
      width: '1024px',
      height: '1366px',
    },
    type: 'tablet'
  },
  {
    name: 'iPad',
    styles: {
      width: '768px',
      height: '1024px',
    },
    type: 'tablet'
  },
  {
    name: 'iPhone X',
    styles: {
      width: '375px',
      height: '812px',
    },
    type: 'mobile'
  },
  {
    name: 'iPhone 6/7/8 plus',
    styles: {
      width: '414px',
      height: '736px',
    },
    type: 'mobile'
  },
  {
    name: 'iPhone 6/7/8',
    styles: {
      width: '375px',
      height: '667px',
    },
    type: 'mobile'
  },
  {
    name: 'iPhone SE',
    styles: {
      width: '320px',
      height: '568px',
    },
    type: 'mobile'
  }
]

export default newViewports
