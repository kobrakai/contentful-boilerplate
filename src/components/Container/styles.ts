import styled from 'styled-components'
import Theme, { Colors } from 'src/theme/index'

interface Props {
  fullWidth?: string
  background?: string
}

export const Wrapper = styled.div<Props>`
  ${({fullWidth}) => `
    max-width: ${fullWidth ? 'none' : Theme.grid.m};
    margin: 0 ${fullWidth ? 0 : Theme.spacing.m};
  `}

  @media (min-width: ${Theme.breakpoints.xl}) {
    margin: 0 auto;
  }
`

export const Colorizer = styled.div<Props>`
  ${({background}) => `
    ${background === 'White' ? `background-color: white;`: ''}
    ${background === 'Gray' ? `background-color: lightgrey;`: ''}
    ${background === 'Purple' ? `background-color: ${Colors.primary.main};`: ''}
    ${background === 'Blue' ? `background-color: ${Colors.secondary.main};`: ''}
  `}
`
