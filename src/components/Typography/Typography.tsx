import React from 'react'
import * as Styles from './styles'

export interface Props {
	tag?: string
	variant?:
		| 'h1'
		| 'h2'
		| 'h3'
		| 'h4'
		| 'h5'
		| 'h6'
		| 'body'
		| 'small'
		| 'label'
		| 'screenreader'
	align?: 'left' | 'center' | 'right' | 'justify'
	color?: string
	children: React.ReactNode
}

const variantMapping = {
	h1: 'h1',
	h2: 'h2',
	h3: 'h3',
	h4: 'h4',
	h5: 'h5',
	h6: 'h6',
	body: 'p',
	label: 'label',
	small: 'small',
	screenreader: 'span'
}

const Typogaphy: React.FC<Props> = ({
	tag = 'div',
	variant = 'body',
	color = 'inherit',
	align = 'left',
	children,
	...rest
}: Props) => (
	<Styles.Tag
		as={tag || variantMapping[variant]}
		variant={variant}
		textcolor={color}
		align={align}
		{...rest}
	>
		{children}
	</Styles.Tag>
)

export default Typogaphy
