import React, { forwardRef } from 'react'
import Typography from 'components/Typography'
import Container from 'components/Container'
import * as Styles from './styles'

interface Props {
  ref: any
}

const Footer: React.FC<Props> = forwardRef<HTMLDivElement, Props>((
  props: Props,
  ref: React.Ref<HTMLDivElement>
) => (
  <Styles.Footer ref={ref}>
    <Container>
      <Typography tag="small">
        ©Maikel Mast {new Date().getFullYear()}
      </Typography>
    </Container>
  </Styles.Footer>
))

export default Footer
