import { Grid, GridProps } from './grid'
import { Breakpoints, BreakpointsProps } from './breakpoints'
import { Spacing, SpacingProps } from './spacing'
import { Typography, TypographyProps } from './typograpgy'
import { Colors, ColorsProps } from './colors'
import globalCss from './globalCss'

interface ThemeProps {
  grid: GridProps
  breakpoints: BreakpointsProps
  spacing: SpacingProps
  typography: TypographyProps
  colors: ColorsProps
}

const Theme: ThemeProps = {
  grid: Grid,
  breakpoints: Breakpoints,
  spacing: Spacing,
  typography: Typography,
  colors: Colors
}

export { ThemeProps, globalCss, Grid, Breakpoints, Spacing, Typography, Colors }
export default Theme
