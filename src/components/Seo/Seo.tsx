import React from 'react'
import { useStaticQuery, graphql } from 'gatsby'
import { Helmet } from 'react-helmet'

interface Props {
	path: string
	title?: string
	lang?: string
	description?: string
	url?: string
	type?: string
	image?: string
	robots?: string
}

const Seo: React.FC<Props> = ({
	path,
	lang,
	title,
	description,
	type,
	url,
	image,
	robots
}: Props) => {
	const {
		site: { siteMetadata }
	} = useStaticQuery(graphql`
		query {
			site {
				siteMetadata {
					title
					description
					author
					siteUrl
					defaultLanguage
					defautlImage
				}
			}
		}
	`)

	const seo = {
		lang: lang || siteMetadata.defaultLanguage,
		title: title || siteMetadata.title,
		description: description || siteMetadata.description,
		image: image
			? `https:${image}`
			: `${siteMetadata.siteUrl}/${siteMetadata.defautlImage}`,
		type: type || 'website',
		url: url || `${siteMetadata.siteUrl}${path}`,
		robots: robots || 'index, follow'
	}

	return (
		<Helmet
			htmlAttributes={{ lang: seo.lang }}
			title={seo.title}
			titleTemplate={`%s | ${siteMetadata.title}`}
		>
			<link rel="canonical" href={seo.url} />
			<meta name="description" content={seo.description} />
			<meta name="robots" content={seo.robots} />
			<meta name="image" content={seo.image} />

			<meta name="twitter:title" content={seo.title} />
			<meta name="twitter:description" content={seo.description} />
			<meta name="twitter:image" content={seo.image} />
			<meta name="twitter:card" content="summary_large_image" />

			<meta property="og:title" content={seo.title} />
			<meta
				prefix="og: http://ogp.me/ns#"
				property="og:url"
				content={seo.url}
			/>
			<meta
				prefix="og: http://ogp.me/ns#"
				property="og:type"
				content={type || 'website'}
			/>
			<meta
				prefix="og: http://ogp.me/ns#"
				property="og:description"
				content={seo.description}
			/>
			<meta
				prefix="og: http://ogp.me/ns#"
				property="og:image"
				content={seo.image}
			/>
		</Helmet>
	)
}

export default Seo
