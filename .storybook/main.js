const path = require('path')

module.exports = {
	stories: [
		'../src/components/**/*.stories.mdx',
		'../src/components/**/*.stories.@(js|jsx|ts|tsx)'
	],
	addons: [
		'@storybook/addon-links',
		'@storybook/addon-essentials',
		'@storybook/addon-a11y',
		'@storybook/addon-storysource'
	],
	webpackFinal: async (config) => {
		config.module.rules[0].exclude = [/node_modules\/(?!(gatsby)\/)/]
		config.module.rules[0].use[0].loader = require.resolve('babel-loader')

		config.module.rules[0].use[0].options.presets = [
			require.resolve('@babel/preset-react'),
			require.resolve('@babel/preset-env')
		]

		config.module.rules[0].use[0].options.plugins = [
			require.resolve('@babel/plugin-proposal-class-properties'),
			require.resolve('babel-plugin-remove-graphql-queries')
		]

		config.resolve.mainFields = ['browser', 'module', 'main']

		config.module.rules.push({
			test: /\.(ts|tsx)$/,
			loader: require.resolve('babel-loader'),
			options: {
				presets: [['react-app', { flow: false, typescript: true }]],
				plugins: [
					require.resolve('@babel/plugin-proposal-class-properties'),
					require.resolve('babel-plugin-remove-graphql-queries')
				]
			}
		})

		config.resolve.extensions.push('.ts', '.tsx')

		config.resolve.alias = {
			...config.resolve.alias,
			src: path.resolve(__dirname, '../src/'),
			components: path.resolve(__dirname, '../src/components/'),
			theme: path.resolve(__dirname, '../src/theme/'),
			images: path.resolve(__dirname, '../src/images/'),
			locales: path.resolve(__dirname, 'src/locales/'),
			i18n: path.resolve(__dirname, 'src/i18n/'),
			queries: path.resolve(__dirname, '../src/queries/'),
			templates: path.resolve(__dirname, '../src/templates/'),
			stories: path.resolve(__dirname, '../stories/')
		}

		return config
	}
}
