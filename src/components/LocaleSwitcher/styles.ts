import styled from 'styled-components'
import { Colors } from 'src/theme/index'

export const Wrapper = styled.div`
  display: flex;

  a {
    color: ${Colors.primary.text};
    margin-right: 1rem;

    &:last-of-type {
      margin-right: 0;
    }
  }
`
