import styled, { css } from 'styled-components'
import { Link as GatsbyLink } from 'gatsby'
import { Colors, ThemeProps } from 'src/theme'

interface Props extends React.AnchorHTMLAttributes<HTMLAnchorElement> {
  theme?: ThemeProps
  size?: 's' | 'm' | 'l'
  variant?: 'primary' | 'secondary' | 'white'
  hascontent?: any
}

const sharedStyles = ({
  variant,
  size,
  hascontent
}: Props) => css`
  cursor: pointer;
  position: relative;
  display: inline-block;
  width: auto;
  transition: background 0.3s ease;
  text-decoration: none;

  ${variant === 'primary' ? `
    background-color: ${Colors.primary.dark};

    span {
      color: ${Colors.primary.text};
    }
  ` : ''}

  ${variant === 'secondary' ? `
    background-color: ${Colors.secondary.dark};

    span {
      color: ${Colors.secondary.text};
    }
  ` : ''}

  ${variant === 'white' ? `
    background-color: white;

    span {
      color: black;
    }
  ` : ''}

  ${size === 's' ? `
    font-size: 0.695rem;
  ` : ''}

  ${size === 'm' ? `
    font-size: 0.834rem;
  ` : ''}

  ${size === 'l' ? `
    font-size: 1rem;
  ` : ''}

  ${hascontent ? `
    padding: 1.125em 5em 1.125em 1.95em;

    &:hover span:last-child {
      width: 100%;

      &::after {
        width: 1.4em;
      }
    }

    span:first-child {
      position: relative;
      z-index: 1;
    }
  ` : `
    padding: 1.75em 1.625em;
    float: right;
  `}
`

export const Button = styled.a<Props>`
  ${sharedStyles}
`

export const Link = styled(GatsbyLink)<Props>`
  ${sharedStyles}
`

export const Arrow = styled.span<Props>`
  position: absolute;
  top: 0;
  right: 0;
  bottom: 0;
  display: inline-block;
  width: 4em;
  transition: width 0.3s ease;

  ${({ variant }) => `
    ${variant === 'primary' ? `
      background-color: ${Colors.primary.main};
    ` : ''}

    ${variant === 'secondary' ? `
      background-color: ${Colors.secondary.main};
    ` : ''}

    ${variant === 'white' ? `
      background-color: #e6e6e6;
    ` : ''}
  `}

  &::before,
  &::after {
    content: '';
    display: block;
    position: absolute;
    top: 50%;

    ${({ variant }) => `
      ${variant === 'primary' ? `
        border-top: 1px solid ${Colors.primary.text};]
      ` : ''}

      ${variant === 'secondary' ? `
        border-top: 1px solid ${Colors.secondary.text};
      ` : ''}

      ${variant === 'white' ? `
        border-top: 1px solid black;
      ` : ''}
    `}
  }

  &::before {
    height: 0.4em;
    width: 0.4em;
    right: 1.4em;
    transform: rotate(45deg);
    transform-origin: 100% 0%;

    ${({ variant, theme }) => `
      ${variant === 'primary' ? `
        border-right: 1px solid ${Colors.primary.text};]
      ` : ''}

      ${variant === 'secondary' ? `
        border-right: 1px solid ${Colors.secondary.text};
      ` : ''}

      ${variant === 'white' ? `
        border-right: 1px solid black;
      ` : ''}
    `}
  }

  &::after {
    right: 1.45em;
    transform: translateY(-50%);
    width: 0.7em;
    transition: width 0.3s ease-out;
  }
`
