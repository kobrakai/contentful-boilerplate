import React from 'react'
import styled from 'styled-components'
import { Story, Meta } from '@storybook/react/types-6-0'
import { Container, ContainerProps, Row, RowProps, Col } from './'

interface Props extends ContainerProps, RowProps {
	columns: number
}

const Wrapper = styled.div`
	display: flex;
	justify-content: center;
	flex-direction: column;
	align-items: center;
	width: 100%;
	height: 100%;

	> a:not(:last-child) {
		margin-bottom: 1rem;
	}
`

const ContainerColor = styled(Container)`
	background-color: rgba(0, 0, 0, 0.1);
`

const RowColor = styled(Row)`
	background-color: rgba(0, 0, 0, 0.1);
	padding: 1em 1em 0;
`

const ColColor = styled(Col)`
	background-color: rgba(0, 0, 0, 0.1);
	padding: 1em;
	border: 1px solid silver;
	margin-bottom: 1em;
`

export default {
	title: 'Components/Grid',
	component: Container,
	subcomponents: { Row, Col }
} as Meta

const Template: Story<Props> = ({ align, justify, columns, ...rest }) => (
	<Wrapper>
		<ContainerColor md>
			<RowColor align={align} justify={justify}>
				{[...Array(columns)].map((column, index) => (
					<ColColor
						key={index}
						style={{
							minHeight: `${
								Math.floor(Math.random() * 80) + 40
							}px`
						}}
						{...rest}
					>
						Column
					</ColColor>
				))}
			</RowColor>
		</ContainerColor>
	</Wrapper>
)

export const Default = Template.bind({})

Default.parameters = {
	docs: {
		description: {
			story: 'Main grid system'
		}
	}
}

Default.args = {
	columns: 4,
	xs: 12,
	sm: 6,
	md: 4,
	lg: 3,
	xl: 2,
	align: 'stretch',
	justify: 'center'
}

Default.argTypes = {
	columns: {
		type: { name: 'number' },
		description: 'Amount of columns in the row',
		control: {
			type: 'range',
			min: 1,
			max: 12,
			step: 1
		}
	},
	xs: {
		type: { name: 'number' },
		description: 'Column width on extra small devices (mobile)',
		control: {
			type: 'range',
			min: 1,
			max: 12,
			step: 1
		}
	},
	sm: {
		type: { name: 'number' },
		description: 'Column width on small devices (tablet)',
		control: {
			type: 'range',
			min: 1,
			max: 12,
			step: 1
		}
	},
	md: {
		type: { name: 'number' },
		description: 'Column width on medium devices (laptop)',
		control: {
			type: 'range',
			min: 1,
			max: 12,
			step: 1
		}
	},
	lg: {
		type: { name: 'number' },
		description: 'Column width on large devices (desktop)',
		control: {
			type: 'range',
			min: 1,
			max: 12,
			step: 1
		}
	},
	xl: {
		type: { name: 'number' },
		description: 'Column width on extra large devices (4K)',
		control: {
			type: 'range',
			min: 1,
			max: 12,
			step: 1
		}
	},
	align: {
		type: { name: 'string' },
		description: 'Row vertical item alignment',
		table: {
			defaultValue: { summary: 'stretch' }
		},
		control: {
			type: 'select',
			options: ['baseline', 'flex-start', 'flex-end', 'center', 'stretch']
		}
	},
	justify: {
		type: { name: 'string' },
		description: 'Row horizontal item alignment',
		table: {
			defaultValue: { summary: 'flex-start' }
		},
		control: {
			type: 'select',
			options: [
				'space-evenly',
				'space-between',
				'space-around',
				'center',
				'flex-start',
				'flex-end'
			]
		}
	}
}
