import React, { useEffect, useState } from 'react'
import Typography from 'components/Typography'
import * as Styles from './styles'

interface Props extends React.AnchorHTMLAttributes<HTMLAnchorElement> {
	to: string
	language: string
	size: 's' | 'm' | 'l'
	variant: 'primary' | 'secondary' | 'white'
	children: any
}

const Button: React.FC<Props> = ({
	language,
	to,
	children,
	size = 'l',
	variant = 'primary',
	...rest
}) => {
	const [parameters, setParameters] = useState({})
	const isInternal =
		to.startsWith(process.env.GATSBY_HOST) ||
		(!to.startsWith('#') && !to.startsWith('http'))

	const ButtonType = ({ children }) => {
		if (isInternal) {
			return (
				<Styles.Link
					to={to}
					size={size}
					variant={variant}
					hascontent={children}
					{...rest}
				>
					{children}
				</Styles.Link>
			)
		} else {
			return (
				<Styles.Button
					size={size}
					variant={variant}
					hascontent={children}
					{...parameters}
					{...rest}
				>
					{children}
				</Styles.Button>
			)
		}
	}

	useEffect(() => {
		const locale =
			!language || language === process.env.GATSBY_DEFAULT_LANGUAGE
				? ''
				: `/${language}`

		if (to.startsWith('http')) {
			setParameters({
				href: to,
				target: '_blank',
				rel: 'noopener noreferrer'
			})
		} else if (to.startsWith('#')) {
			setParameters({ href: to })
		} else if (to.startsWith('/')) {
			setParameters({
				href: `${locale}${to}`
			})
		} else {
			setParameters({
				href: `${locale}/${to}`
			})
		}
	}, [to, language])

	return (
		<ButtonType>
			{children && (
				<Typography tag="span" variant="label">
					{children}
				</Typography>
			)}

			<Styles.Arrow variant={variant} />
		</ButtonType>
	)
}

export default Button
