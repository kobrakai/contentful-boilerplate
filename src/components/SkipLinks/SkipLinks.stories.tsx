import React from 'react'
import { Meta } from '@storybook/react/types-6-0'
import SkipLinks from './SkipLinks'

export default {
	title: 'Components/Skip links',
	component: SkipLinks
} as Meta

export const Default = () => <SkipLinks />

Default.parameters = {
	title: 'Skip links',
	subtitle: 'Tab skip links for accessibility',
	docgen: ''
}
