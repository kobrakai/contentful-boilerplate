import React from 'react'
import renderer from 'react-test-renderer'
import Layout from 'components/Layout'
import '../../i18n/index'

describe('Layout', () => {
	it('renders correctly', () => {
		const path = '/'
		const context = {
			id: 'fec91240-366c-519d-aa1b-e2785b67df15',
			locale: 'en'
		}

		const tree = renderer
			.create(
				<Layout path={path} context={context}>
					Test content
				</Layout>
			)
			.toJSON()

		expect(tree).toMatchSnapshot()
	})
})
