import React from 'react'
import * as Styles from './styles'

export interface RowProps extends React.AnchorHTMLAttributes<HTMLDivElement> {
  align?: 'baseline'
  | 'flex-start'
  | 'flex-end'
  | 'center'
  | 'stretch'
  justify?: 'space-evenly'
  | 'space-between'
  | 'space-around'
  | 'center'
  | 'flex-start'
  | 'flex-end'
  children: any
}

export const Row: React.FC<RowProps> = ({
  children,
  ...rest
}) => (
  <Styles.Row {...rest}>
    {children}
  </Styles.Row>
)

Row.defaultProps = {
  align: 'stretch',
  justify: 'flex-start'
}

export default Row




  