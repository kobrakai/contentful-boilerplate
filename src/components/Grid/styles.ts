import styled, { css } from 'styled-components'
import { ThemeProps } from 'src/theme'
import { ContainerProps, RowProps, ColProps } from './'

export type ContainerInterface = ContainerProps & ThemeProps

export const Container = styled.div<ContainerInterface> `
  padding-right: ${({ theme }) => theme.spacing.m};
  padding-left: ${({ theme }) => theme.spacing.m};

  ${({ theme, md }) => md && css `
    width: 100%;
    max-width: ${theme.grid.m};
    margin: 0 auto;
  `}

  ${({ theme, md }) => md && css `
    width: 100%;
    max-width: ${theme.grid.m};
    margin: 0 auto;
  `}
`

export const Row = styled.div<RowProps> `
  display: flex;
  flex-wrap: wrap;
  margin-right: -15px;
  margin-left: -15px;
  justify-content: ${({ justify }) => justify};
  align-items: ${({ align }) => align};
`

export const Col = styled.div<ColProps> `
  position: relative;
  width: 100%;
  padding-right: 15px;
  padding-left: 15px;

  ${({ xs })  => xs && css `
    @media only screen and (min-width: 320px) {
      ${({ xs }: ColProps) => typeof xs === 'number' && css `
        flex: 0 0 ${((100 / 12) * xs).toFixed(4)}%;
        max-width: ${((100 / 12) * xs).toFixed(4)}%;
      `}

      ${({ xs }: ColProps) => xs.span && css `
        flex: 0 0 ${((100 / 12) * xs.span).toFixed(4)}%;
        max-width: ${((100 / 12) * xs.span).toFixed(4)}%;
      `}
      
      ${({ xs }: ColProps) => typeof xs !== 'number' && !xs.span && css `
        flex-basis: 0;
        flex-grow: 1;
        max-width: 100%;
      `}

      ${({ xs }: ColProps) => xs.offset && css `
        margin-left: ${((100 / 12) * xs.offset).toFixed(4)}%;
      `}
    }
  `}

  ${({ sm }) => sm && css `
    @media only screen and (min-width: 567px) {
      ${({ sm }: ColProps) => typeof sm === 'number' && css `
        flex: 0 0 ${((100 / 12) * sm).toFixed(4)}%;
        max-width: ${((100 / 12) * sm).toFixed(4)}%;
      `}

      ${({ sm }: ColProps) => sm.span && css `
        flex: 0 0 ${((100 / 12) * sm.span).toFixed(4)}%;
        max-width: ${((100 / 12) * sm.span).toFixed(4)}%;
      `}
      
      ${({ sm }: ColProps) => typeof sm !== 'number' && !sm.span && css `
        flex-basis: 0;
        flex-grow: 1;
        max-width: 100%;
      `}

      ${({ sm }: ColProps) => sm.offset && css `
        margin-left: ${((100 / 12) * sm.offset).toFixed(4)}%;
      `}
    }
  `}

  ${({ md }) => md && css `
    @media only screen and (min-width: 1024px) {
      ${({ md }: ColProps) => typeof md === 'number' && css `
        flex: 0 0 ${((100 / 12) * md).toFixed(4)}%;
        max-width: ${((100 / 12) * md).toFixed(4)}%;
      `}

      ${({ md }: ColProps) => md.span && css `
        flex: 0 0 ${((100 / 12) * md.span).toFixed(4)}%;
        max-width: ${((100 / 12) * md.span).toFixed(4)}%;
      `}
      
      ${({ md }: ColProps) => typeof md !== 'number' && !md.span && css `
        flex-basis: 0;
        flex-grow: 1;
        max-width: 100%;
      `}

      ${({ md }: ColProps) => md.offset && css `
        margin-left: ${((100 / 12) * md.offset).toFixed(4)}%;
      `}
    }
  `}

  ${({ lg }) => lg && css `
    @media only screen and (min-width: 1200px) {
      ${({ lg }: ColProps) => typeof lg === 'number' && css `
        flex: 0 0 ${((100 / 12) * lg).toFixed(4)}%;
        max-width: ${((100 / 12) * lg).toFixed(4)}%;
      `}

      ${({ lg }: ColProps) => lg.span && css `
        flex: 0 0 ${((100 / 12) * lg.span).toFixed(4)}%;
        max-width: ${((100 / 12) * lg.span).toFixed(4)}%;
      `}
      
      ${({ lg }: ColProps) => typeof lg !== 'number' && !lg.span && css `
        flex-basis: 0;
        flex-grow: 1;
        max-width: 100%;
      `}

      ${({ lg }: ColProps) => lg.offset && css `
        margin-left: ${((100 / 12) * lg.offset).toFixed(4)}%;
      `}
    }
  `}

  ${({ xl }) => xl && css `
    @media only screen and (min-width: 1400px) {
      ${({ xl }: ColProps) => typeof xl === 'number' && css `
        flex: 0 0 ${((100 / 12) * xl).toFixed(4)}%;
        max-width: ${((100 / 12) * xl).toFixed(4)}%;
      `}

      ${({ xl }: ColProps) => xl.span && css `
        flex: 0 0 ${((100 / 12) * xl.span).toFixed(4)}%;
        max-width: ${((100 / 12) * xl.span).toFixed(4)}%;
      `}
      
      ${({ xl }: ColProps) => typeof xl !== 'number' && !xl.span && css `
        flex-basis: 0;
        flex-grow: 1;
        max-width: 100%;
      `}

      ${({ xl }: ColProps) => xl.offset && css `
        margin-left: ${((100 / 12) * xl.offset).toFixed(4)}%;
      `}
    }
  `}
`
