/**
 * Implement Gatsby's Node APIs in this file.
 *
 * See: https://www.gatsbyjs.org/docs/node-apis/
 */

const path = require('path')
const fs = require(`fs`)
const customTypeDefs = fs.readFileSync('type-defs.gql', { encoding: 'utf-8' })

const getLocalePath = (locale, slug) => {
	const language =
		locale.split('-')[0] === process.env.GATSBY_DEFAULT_LANGUAGE
			? ''
			: locale.split('-')[0]
	const path = slug === '/' ? '' : slug
	const cleanPath = path.startsWith('/') ? path.split('/')[1] : path

	return `${language}/${cleanPath}`
}

exports.sourceNodes = ({ actions }) => {
	const { createTypes } = actions
	const typeDefs = customTypeDefs

	createTypes(typeDefs)
}

exports.createPages = async ({ graphql, actions: { createPage } }) => {
	const page = graphql(`
		{
			site {
				siteMetadata {
					title
					description
					author
					siteUrl
					defaultLanguage
					defautlImage
				}
			}

			page: allContentfulPage(limit: 100) {
				edges {
					node {
						id
						slug
						node_locale
					}
				}
			}
		}
	`).then((result) => {
		if (result.errors) {
			Promise.reject(result.errors)
		}

		const metadata = result.data.site.siteMetadata

		result.data.page.edges
			.map((edge) => edge.node)
			.forEach(
				({
					id,
					node_locale,
					slug
				}) => {
					createPage({
						path: getLocalePath(node_locale, slug),
						component: path.resolve('src/templates/Page.tsx'),
						context: {
							id,
							slug,
							metadata
						}
					})
				}
			)
	})

	return Promise.all([page])
}
