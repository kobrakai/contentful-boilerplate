import React from 'react'
import { addParameters } from '@storybook/client-api'
import { action } from '@storybook/addon-actions'
import styled, { ThemeProvider, createGlobalStyle } from 'styled-components'
import { Normalize } from 'styled-normalize'
import Theme, { globalCss } from '../src/theme'
import { default as newViewports } from './viewport'
import '../src/i18n/index'

const GlobalStyles = createGlobalStyle`${globalCss}`
const Wrapper = styled.div`
	padding-top: 40px;
	padding-bottom: 40px;
`

export const parameters = {
	actions: { argTypesRegex: '^on[A-Z].*' }
}

export const decorators = [
	(Story) => (
		<ThemeProvider theme={Theme}>
			<Normalize />
			<GlobalStyles />
			<Wrapper>
				<Story />
			</Wrapper>
		</ThemeProvider>
	)
]

addParameters({
	viewport: {
		viewports: newViewports,
		defaultViewport: 'Responsive'
	}
})

global.___loader = {
	enqueue: () => {},
	hovering: () => {}
}

global.__BASE_PATH__ = ''
global.__PATH_PREFIX__ = ''

window.___navigate = (pathname) => {
	action('NavigateTo:')(pathname)
}
