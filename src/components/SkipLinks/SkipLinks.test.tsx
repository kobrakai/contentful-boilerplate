import React from 'react'
import renderer from 'react-test-renderer'
import SkipLinks from 'components/SkipLinks'
import '../../i18n/index'

describe('Skip links', () => {
	it('renders correctly', () => {
		const tree = renderer.create(<SkipLinks />).toJSON()
		expect(tree).toMatchSnapshot()
	})
})
