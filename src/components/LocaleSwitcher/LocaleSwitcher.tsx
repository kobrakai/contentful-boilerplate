import React from 'react'
import { useTranslation } from 'react-i18next'
import Link from 'components/Link'
import * as Styles from './styles'

interface Props {
	path: string
	context: {
		id: string
		locale: string
	}
}

const LocaleSwitcher: React.FC<Props> = ({ path, context, ...rest }: Props) => {
	const { t, i18n } = useTranslation()

	return (
		<Styles.Wrapper {...rest}>
			{i18n.languages.sort().map((language) => (
				<Link
					key={`language-switch-${language}`}
					to={
						process.env.GATSBY_DEFAULT_LANGUAGE === language
							? '/'
							: `/${language}`
					}
				>
					{t(language)}
				</Link>
			))}
		</Styles.Wrapper>
	)
}

export default LocaleSwitcher
