/**
 * Implement Gatsby's Browser APIs in this file.
 *
 * See: https://www.gatsbyjs.org/docs/browser-apis/
 */

import React from 'react'
import { ThemeProvider, createGlobalStyle } from 'styled-components'
import { Normalize } from 'styled-normalize'
import Theme, { globalCss } from './src/theme'
import './src/i18n/index'

const GlobalStyles = createGlobalStyle`${globalCss}`

export const wrapRootElement = ({ element }) => (
	<ThemeProvider theme={Theme}>
		<Normalize />
		<GlobalStyles />
		{element}
	</ThemeProvider>
)
