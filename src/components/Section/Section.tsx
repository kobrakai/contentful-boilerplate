import React from 'react'
import ComponentList from 'components/ComponentList'
import Container from 'components/Container'
import Typography from 'components/Typography'

interface Props {
	title: string
	background?: string
	alignment?: 'Left' | 'Center' | 'Right'
	content: Array<any>
}

const Section: React.FC<Props> = ({
	title,
	background,
	alignment,
	content,
	...rest
}: Props) => (
	<Container background={background} {...rest}>
		<Typography tag="h2" variant="screenreader">
			{title}
		</Typography>
		<ComponentList components={content} />
	</Container>
)

export default Section
