import React from 'react'
import { Story, Meta } from '@storybook/react/types-6-0'
import Typography, { Props } from './Typography'

interface StoryProps extends Props {
	text: string
}

const args = {
	text: 'Some testing text',
	tag: 'div',
	align: 'left',
	color: 'inherit'
}

const argTypes = {
	variant: {
		type: { name: 'string', required: true },
		description: 'Typography styling',
		table: {
			defaultValue: { summary: 'body' }
		},
		control: {
			type: 'select',
			options: [
				'h1',
				'h2',
				'h3',
				'h4',
				'h5',
				'h6',
				'body',
				'small',
				'label',
				'screenreader'
			]
		}
	},
	tag: {
		type: { name: 'string', required: true },
		description: 'HTML tag element',
		table: {
			defaultValue: { summary: 'div' }
		},
		control: {
			type: 'select',
			options: [
				'h1',
				'h2',
				'h3',
				'h4',
				'h5',
				'h6',
				'div',
				'span',
				'strong',
				'italic'
			]
		}
	},
	align: {
		type: { name: 'string' },
		description: 'Text alignment',
		table: {
			defaultValue: { summary: 'left' }
		},
		control: {
			type: 'select',
			options: ['left', 'center', 'right', 'justify']
		}
	},
	color: {
		type: { name: 'string' },
		description: 'Text color',
		table: {
			defaultValue: { summary: 'inherit' }
		},
		control: { type: 'color' }
	}
}

export default {
	title: 'Components/Typography',
	component: Typography
} as Meta

const Template: Story<StoryProps> = ({ text, ...rest }) => (
	<Typography {...rest}>{text}</Typography>
)

export const H1 = Template.bind({})
H1.args = { ...args, variant: 'h1' }
H1.argTypes = argTypes

export const H2 = Template.bind({})
H2.args = { ...args, variant: 'h2' }
H2.argTypes = argTypes

export const H3 = Template.bind({})
H3.args = { ...args, variant: 'h3' }
H3.argTypes = argTypes

export const H4 = Template.bind({})
H4.args = { ...args, variant: 'h4' }
H4.argTypes = argTypes

export const H5 = Template.bind({})
H5.args = { ...args, variant: 'h5' }
H5.argTypes = argTypes

export const H6 = Template.bind({})
H6.args = { ...args, variant: 'h6' }
H6.argTypes = argTypes

export const Body = Template.bind({})
Body.args = { ...args, variant: 'body' }
Body.argTypes = argTypes

export const Small = Template.bind({})
Small.args = { ...args, variant: 'small' }
Small.argTypes = argTypes

export const Label = Template.bind({})
Label.args = { ...args, variant: 'label' }
Label.argTypes = argTypes

export const Screenreader = Template.bind({})
Screenreader.args = { ...args, variant: 'screenreader' }
Screenreader.argTypes = argTypes
