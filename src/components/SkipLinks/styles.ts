import React from 'react'
import styled from 'styled-components'
import Button from 'components/Button'

interface Props extends React.AnchorHTMLAttributes<HTMLAnchorElement> {
  size?: 's' | 'm' | 'l'
  variant?: 'primary' | 'secondary' | 'white'
}

export const Wrapper = styled.div`
  position: absolute;
  top: 2rem;
  width: 15.5rem;
`
export const Link = styled(Button)<Props>`
  pointer-events: none;
  position: absolute;
  top: 0;
  left: -0.25rem;
  min-width: 12rem;
  margin-top: 0;
  opacity: 0;

  &:first-child {
    position: relative;
  }

  &:focus {
    opacity: 1;
    z-index: 15;
  }
`
