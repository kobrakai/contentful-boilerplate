import React from 'react'
import renderer from 'react-test-renderer'
import Footer from 'components/Footer'
import '../../i18n/index'

describe('Footer', () => {
	it('renders correctly', () => {
		const tree = renderer.create(<Footer ref={{ current: {} }} />).toJSON()
		expect(tree).toMatchSnapshot()
	})
})
