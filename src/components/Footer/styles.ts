import styled from 'styled-components'
import { ThemeProps } from 'theme/index'

interface Props {
	theme: ThemeProps
}

export const Footer = styled.footer<Props>`
	padding: 6.25em 0;
	background-color: lightgray;
`
