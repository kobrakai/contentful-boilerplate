import React, { useState, useEffect, forwardRef } from 'react'
import { useTranslation } from 'react-i18next'
import LocaleSwitcher from 'components/LocaleSwitcher'
import Typography from 'components/Typography'
import * as Styles from './styles'

interface Props {
	ref: any
	path: string
	context: {
		id: string
		locale: string
	}
}

const Header: React.FC<Props> = forwardRef<HTMLDivElement, Props>(
	({ path, context }: Props, ref: React.Ref<HTMLDivElement>) => {
		const { i18n } = useTranslation()
		const [url, setUrl] = useState('/')

		useEffect(() => {
			setUrl(
				i18n.language === process.env.GATSBY_DEFAULT_LANGUAGE
					? '/'
					: `/${i18n.language}`
			)
		}, [i18n.language])

		return (
			<Styles.Header ref={ref}>
				<Styles.Container>
					<Styles.Logo to={url}>
						<Typography tag="span" variant="h6">
							Boilerplate
						</Typography>
					</Styles.Logo>

					<LocaleSwitcher path={path} context={context} />
				</Styles.Container>
			</Styles.Header>
		)
	}
)

export default Header
