import React from 'react'
import * as Styles from './styles'

interface Props {
  background?: string
  fullWidth?: string
  children: React.ReactNode
}

const Container: React.FC<Props> = ({ 
  background,
  fullWidth,
  children,
  ...rest
}: Props) => (
  <Styles.Colorizer background={background}>
    <Styles.Wrapper fullWidth={fullWidth} {...rest}>
      {children}
    </Styles.Wrapper>
  </Styles.Colorizer>
)

export default Container
