import React, { useEffect } from 'react'
import { useTranslation } from 'react-i18next'
import { graphql } from 'gatsby'
import Hero from 'components/Hero'
import Layout from 'components/Layout'
import Seo from 'components/Seo'
import { PageFragmentFragment } from 'types/graphql-types'

interface PageContextProps extends PageFragmentFragment {
	locale: string
}

interface Props {
	data: { contentfulPage: PageFragmentFragment }
	pageContext: PageContextProps
	path: string
}

const Page: React.FC<Props> = ({ data, path, pageContext }: Props) => {
	const { i18n } = useTranslation()
	const { locale } = pageContext
	const {
		title,
		richDescription,
		image,
		metaTitle,
		metaDescription,
		metaRobots
	} = data.contentfulPage

	useEffect(() => {
		i18n.changeLanguage(locale)
	}, [pageContext])

	return (
		<Layout path={path} context={pageContext}>
			<Seo
				title={metaTitle}
				description={metaDescription.metaDescription}
				robots={metaRobots}
				path={path}
			/>

			{title && image && (
				<Hero
					title={title}
					content={richDescription}
					image={image}
				/>
			)}
		</Layout>
	)
}

export const query = graphql`
	query($id: String!) {
		contentfulPage(id: {eq: $id}) {
			...PageFragment
		}
	}
`

export default Page
