import i18n from 'i18next'
import { initReactI18next } from 'react-i18next'

i18n.use(initReactI18next).init({
	lng: process.env.GATSBY_DEFAULT_LANGUAGE,
	fallbackLng: ['nl', 'en'],
	preload: ['nl', 'en'],
	resources: {
		nl: {
			translations: require('../locales/nl/translations.json')
		},
		en: {
			translations: require('../locales/en/translations.json')
		}
	},
	ns: ['translations'],
	defaultNS: 'translations',
	debug: process.env.NODE_ENV === 'development',
	keySeparator: false,
	interpolation: {
		escapeValue: false
	},
	react: {
		wait: true
	}
})

i18n.languages = ['nl', 'en']

export default i18n
