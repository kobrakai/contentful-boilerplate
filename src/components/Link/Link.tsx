import React, { useState, useEffect } from 'react'
import * as Styles from './styles'

export interface Props {
	language?: string
	to: string
	children: any
}

const Link = ({ language, to, children, ...rest }: Props) => {
	const [parameters, setParameters] = useState({})
	const isInternal =
		to.startsWith(process.env.GATSBY_HOST) ||
		(!to.startsWith('#') && !to.startsWith('http'))

	const LinkType = ({ children }) => {
		if (isInternal) {
			return (
				<Styles.InternalLink to={to} {...rest}>
					{children}
				</Styles.InternalLink>
			)
		} else {
			return (
				<Styles.ExternalLink {...parameters} {...rest}>
					{children}
				</Styles.ExternalLink>
			)
		}
	}

	useEffect(() => {
		const locale =
			!language || language === process.env.GATSBY_DEFAULT_LANGUAGE
				? ''
				: `/${language}`

		if (to.startsWith('http')) {
			setParameters({
				href: to,
				target: '_blank',
				rel: 'noopener noreferrer'
			})
		} else if (to.startsWith('#')) {
			setParameters({ href: to })
		} else if (to.startsWith('/')) {
			setParameters({
				href: `${locale}${to}`
			})
		} else {
			setParameters({
				href: `${locale}/${to}`
			})
		}
	}, [to, language])

	return <LinkType {...rest}>{children}</LinkType>
}

export default Link
