import styled from 'styled-components'
import { Link } from 'gatsby'
import { default as BaseContainer } from 'components/Container'
import { Colors } from 'src/theme/index'

export const Container = styled(BaseContainer)`
	display: flex;
	justify-content: space-between;
	align-items: center;
`

export const Header = styled.header`
	position: absolute;
	top: 0;
	right: 0;
	left: 0;
	background-color: ${Colors.primary.main};
	color: ${Colors.primary.text};
	padding: 1.25em 0;
`

export const Logo = styled(Link)`
	color: ${Colors.primary.text};
	text-decoration: none;
`
