/**
 * Configure your Gatsby site with this file.
 *
 * See: https://www.gatsbyjs.org/docs/gatsby-config/
 */

require('dotenv').config({ path: '.env' })
const path = require('path')

module.exports = {
	siteMetadata: {
		title: process.env.PROJECT_SHORT_TITLE,
		description: process.env.PROJECT_DESCRIPTION,
		author: process.env.PROJECT_AUTHOR,
		siteUrl: process.env.GATSBY_HOST,
		defaultLanguage: 'en',
		defautlImage: 'landing.png'
	},
	plugins: [
		'gatsby-plugin-react-helmet',
		'gatsby-plugin-typescript',
		'gatsby-plugin-styled-components',
		'gatsby-plugin-sitemap',
		'gatsby-plugin-sharp',
		'gatsby-transformer-sharp',
		'gatsby-plugin-offline',
		{
			resolve: 'gatsby-source-filesystem',
			options: {
				name: 'images',
				path: `${__dirname}/src/images`
			}
		},
		{
			resolve: 'gatsby-plugin-manifest',
			options: {
				name: process.env.PROJECT_TITLE,
				short_name: process.env.PROJECT_SHORT_TITLE,
				description: process.env.PROJECT_DESCRIPTION,
				lang: 'nl',
				icon: 'src/images/favicon.svg',
				start_url: '/',
				background_color: '#f7f0eb',
				theme_color: '#663399',
				display: 'standalone'
			}
		},
		{
			resolve: 'gatsby-plugin-robots-txt',
			options: {
				host: process.env.GATSBY_HOST,
				sitemap: `${process.env.GATSBY_HOST}/sitemap.xml`,
				env: {
					development: {
						policy: [{ userAgent: '*', disallow: ['/'] }]
					},
					production: {
						policy: [{ userAgent: '*', allow: '/' }]
					}
				}
			}
		},
		{
			resolve: 'gatsby-source-contentful',
			options: {
				spaceId: process.env.CONTENTFUL_SPACE_ID,
				accessToken: process.env.CONTENTFUL_ACCESS_TOKEN,
				environment: process.env.CONTENTFUL_ENVIRONMENT
					? process.env.CONTENTFUL_ENVIRONMENT
					: 'master'
			}
		},
		{
			resolve: 'gatsby-plugin-graphql-codegen',
			options: {
				fileName: 'types/graphql-types.d.ts'
			}
		},
		{
			resolve: 'gatsby-plugin-root-import',
			options: {
				src: `${path.join(__dirname, 'src')}`,
				components: `${path.join(__dirname, 'src/components')}`,
				templates: `${path.join(__dirname, 'src/templates')}`,
				pages: `${path.join(__dirname, 'src/pages')}`,
				theme: `${path.join(__dirname, 'src/theme')}`,
				types: `${path.join(__dirname, 'types')}`
			}
		}
	]
}
