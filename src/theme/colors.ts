interface ColorsProps {
  [key: string]: ColorList
}

interface ColorList {
  [key: string]: string
}

const Colors: ColorsProps = {
  primary: {
    dark: '#f9Cf84',
    main: '#fcaf58',
    light: '#ff8c42',
    text: '#523023'
  },
  secondary: {
    dark: '#26a195',
    main: '#2ec4b6',
    light: '#7ad9d0',
    text: '#1f2d3d'
  }
}

export { ColorsProps, Colors }
