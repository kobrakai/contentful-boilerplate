import React from 'react'
import { Document } from '@contentful/rich-text-types'
import { documentToReactComponents, Options } from '@contentful/rich-text-react-renderer'
import Typography from 'components/Typography'

interface Props {
  title?: string,
  heading?: 'h1'
  | 'h2'
  | 'h3'
  | 'h4'
  | 'h5'
  | 'h6'
  content: Document
}

const RichText: React.FC<Props> = ({
  heading = 'h2',
  title,
  content,
  ...rest
}: Props) => {
  const options: Options = {}

  return (
    <div {...rest}>
      {title && (
        <Typography variant={heading}>
          {title}
        </Typography>
      )}

      <Typography variant="body" tag="div">
        {documentToReactComponents(content, options)}
      </Typography>
    </div>
  )
}

export default RichText
