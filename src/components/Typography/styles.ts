import styled from 'styled-components'
import { Styles } from 'src/theme/typograpgy'

interface Props {
  variant?: string
  align?: string
  textcolor?: string
}

export const Tag = styled.div`
  ${({ variant, align, textcolor }: Props) => (`
    ${variant ? Styles[variant] : ''}
    display: ${ align ? 'block' : 'inherit'};
    color: ${textcolor};
    text-align: ${align};
  `)}
`
