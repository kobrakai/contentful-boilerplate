interface GridProps {
  [key: string]: string
}

const Grid: GridProps = {
  s: '61.875rem', // 996px
  m: '75rem', // 1152px
  l: '90rem', // 1440px
  xl: '100rem' // 1600px
}

export { GridProps, Grid }
